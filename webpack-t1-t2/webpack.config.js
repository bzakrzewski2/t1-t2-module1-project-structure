const path = require('path');
module.exports = {
  context: __dirname, // root of the project
  entry: {
    index: './src/index.js',
    // you can add another file here
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
    ],
  },
};
