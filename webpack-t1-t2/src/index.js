import { addParagraph } from './another-file';

const changeHeader = () => {
  document.querySelector('h1').innerText = 'Webpack!';
};

changeHeader();
addParagraph();
